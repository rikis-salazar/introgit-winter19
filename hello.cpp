#include <iostream>

// Avoid full namespaces!!!

// Crazy idea!!!
// Want to use a function to display a message to either the console, or to an
// output file.


// Yet one more change....
// jhsgdkahdsggkjfa


void print_message(std::string m){
    std::cout << m << '\n';
}

int main(){

    // This caused a conflict
    std::cout << "Argc = " << argc << '\n';
    std::cout << "Hello, world!\n";

    std::string m = "Hello, world!\n";
    print_message(m);      // <-- Should send the info to the console

    // std::ofstream fout("the_file.txt");
    //
    // print_message(m,fout);   // <-- Sends message to file.
    //
    // fout.close();

    return 0;
}


/* 
    OUTPUT: 
        Hello, world!
*/
